import json
import os
import sys
import re
from pathlib import Path

from data_classes.RequestData import RequestData
from regular_expressions import request_name_version


def main() -> None:
    command = sys.argv[1]

    if command == 'validate':
        package_path = Path(sys.argv[2])
        request_name = sys.argv[3]

        print(f'> {command} [{package_path}|{request_name}]')

        validate_version(request_name, package_path)

    elif command == 'read':
        package_path = Path(sys.argv[2])

        print(f'> {command} [{package_path}]')

        version = get_package_version(package_path / 'package.json')

        print(version)  # needed as an output

    else:
        print(f'Wrong input: <{command}>')
        sys.exit(1)


def validate_version(request_name: str, package_path: Path) -> None:
    is_valid = validate_version_match(request_name, package_path)

    if is_valid:
        sys.exit(0)
    else:
        sys.exit(1)


def validate_version_match(request_name: str, package_path: Path) -> bool:
    print("Validating version info")

    request_version = validate_request_name(request_name)
    if not request_version.is_valid:
        return False

    if not validate_package_version(package_path / 'package.json', request_version.version):
        return False

    return True


def validate_request_name(request_name: str) -> RequestData:

    match = re.match(request_name_version, request_name)

    if match is None:
        print(f'Can\'t parse version in request named {request_name}')
        return RequestData(False, None)

    version = match.group(1)
    print(f'Version in request name is: {version}')
    return RequestData(True, version)


def validate_package_version(package_json_path: Path, request_version: str) -> bool:
    is_same = request_version == get_package_version(package_json_path)

    if not is_same:
        print('Package version and request version aren\'t equal!')

    return is_same


def get_package_version(package_json_path: Path) -> str:
    package_json_file = package_json_path.open()
    package_json = json.load(package_json_file)
    package_json_file.close()

    package_version = package_json['version']
    print(f'Package version is {package_version}')

    return package_version


if __name__ == '__main__':
    main()
