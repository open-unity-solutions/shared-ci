from dataclasses import dataclass
from typing import Optional


@dataclass
class RequestData:
    is_valid: bool
    version: Optional[str]
