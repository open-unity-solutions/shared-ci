## [1.2.0] - 2023-06-26

### Changed
- `ci.yml` splat to 3 files: 
  - `ci-common` for common logic
  - `ci-tag-validate-deploy` for tag request naming validation and setting tag on merge commit
  - `ci-registry-deploy` for deploy package to upm registry after merge
- Readme updated
- Licence year updated


## [1.1.1]

### Fixed
- After-merge commits now really run only after merge
- Changelogs refactored


## [1.1.0]

### Added
- Auto-tags after merge, so release can be used as git package in Unity

### Changed
- Readme updated slightly


## [1.0.2]

### Added
- No more submodule required: ci itself clones needed scripts

### Changed
- `.gitlab-ci.yml` renamed to `ci.yml`, so this repository someday  can have own pipelines 


## [1.0.1]

### Added
- Custom docker image implemented
- Executes as a submodule

### Changed
- Job condition is moved to variable


## [1.0.0]

### Added
- Verification, that package version matches the merge request prefix
- Automated package deploy to custom npm registry 
