# Modular CI for Unity modules

## Pipeline files

### `ci-common.yml`

No special pipelines, but needed for other pipelines to work.

### `ci-tag-validate-deploy.yml`

Ensures, that you:
- Named MR with [vX.Y.Z] prefix
- Updated `package.json` version accordingly

After successful merge, adds manual pipeline to mark merge commit with tag `vX.Y.Z`.

### `ci-registry-deploy.yml`

After successful merge, adds manual pipeline to deploy package to UPM registry.


## How to integrate to your CI

It may be integrated to other projects as a gitlab include.

Simple `.gitlab-ci.yml` look like this:
```yaml
include:
  project: 'valhalla-for-unity/shared-ci'
  ref: master
  file:
    - 'ci-common.yml'
    - 'ci-OPTIONAL-PIPELINES.yml'
    - 'ci-MORE-OPTIONAL-PIPELINES.yml' 
```



## Dependancies

This CI written with gitlab-runners-in-docker in mind.

Dockerfile is located in `docker_image/` folder, so you can reuse it.

CI also uses pre-installed on image `python3`.


## Environment variables

All requered variables are listed in ci files.


[ ! ] Make sure you masked your private data, so nobody can steal it.
